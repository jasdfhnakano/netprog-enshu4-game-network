# README of this Project #

This project is forked from [this](https://bitbucket.org/jasdfhnakano/netprog-enshu4-game-nonnetwork).

## What kind of Game? ##

This game is 20x20 grid treasure hunting game.

The specification is the following.

* Game Server must start it first
* Hookup with 2 players(client) for one game.
* 2 players will be placed randomly
* Players can go only 4 direction, North, West, East, South
* On the grid, there are multiple obstacle which blocks the player's direction.
* Players will look for a treasures around the grid.
* When players overlap the same grid, they guess one's number
* When treasure is cleared from the grid, the game is over.

## Game's rule ##

* Player can move North, West, East, and South
* Overlap on a treasure will add score of Player
* Mini Game(Num Guess) happens when Player overlap each other.(Winner gets 999 points)
* When amount of treasure reaches zero, the game is over, the highest score wins.

## How to Play ##
Import this project to you favorite IDE.
After importing, Generate jar execution file for Server and Client.

To run Server
java -jar <Name of Server Packages(.jar)>

To run Client
java -jar <Name of Client Packages(.jar)> <Server Hostname or Server IP Address>


## Why did this project get managed by Git? ##

This project was managed by Git for several reasons.

* Ease of tracking the changes and modification.
* Ease of testing new ideas from any point of commit.
* Public show off of what developer can do.
* Online backup of the project
* Multiple computer access coding.

But the biggest reason of using Git was to show the benefit of using Git as a University's Project.
