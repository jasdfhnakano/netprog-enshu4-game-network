package GridGame_Network_Client;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by spaism on 2/20/15.
 */
public class GridGameClient {

    // Game transaction through PORT 60033
    static final int PORT = 60033;
    // Amount of Buffer size, minimum is best
    static final int BUFSIZ = 128;

    protected String HostName;

    protected InputStream Receive;
    protected OutputStream Send;

    protected Socket socket;

    protected Scanner scan;

    protected int id;

    public GridGameClient(String hostname){

        HostName = hostname;

        scan = new Scanner(System.in);

    }

    public void connect(){

        try{
            socket = new Socket(HostName, PORT);
            Receive = socket.getInputStream();
            Send = socket.getOutputStream();
        }catch(Exception e){
            System.out.println("Connection Error");
            System.exit(1);
        }

    }

    public void doGame(){

        int n;
        byte[] buffer = new byte[BUFSIZ];

        try{
            System.out.println("Waiting for other player...");
            while((n = Receive.read(buffer)) >= 0){
                String signal = new String(buffer, 0, n, "UTF-8");
                //System.out.println(signal);
                if(signal.charAt(0) == '6'){
                    break;
                }else{
                    id = Character.getNumericValue(signal.charAt(0));
                    System.out.println("id: " + id);
                }
            }
            while((n = Receive.read(buffer)) >= 0){
               String signal = new String(buffer, 0, n, "UTF-8");
               //System.out.println(signal);
               doProcess(signal);
            }
        }catch(Exception e){

        }
    }

    public void doProcess(String signal){

        // if 3 then choose Direction to move
        if(signal.charAt(0) == '3'){
            int[] direction = new int[4];

            for(int i = 0; i < 4; i++){
                direction[i] = Character.getNumericValue(signal.charAt(1+i));
            }
            System.out.println("Your Score: " + signal.substring(5));
            chooseDirection(direction);
        }

        // if 4 then overlapped
        if(signal.charAt(0) == '4'){
            System.out.println("You have encountered the overlap!");

            String value = "";
            boolean validate = false;
            while(!validate){
                System.out.print("Choose a number from 0 to 9: ");
                value = scan.next();
                // Catch when user input other than integer
                try{
                    if(Integer.parseInt(value) >= 0 && Integer.parseInt(value) <= 9){
                        validate = true;
                    }
                }catch(NumberFormatException e){
                    validate = false;
                }
            }

            String buffer = String.valueOf(value);

            try{
                Send.write(Arrays.copyOf(buffer.getBytes(), buffer.length() + 1));
            }catch(Exception e){
                e.printStackTrace();
            }
        }

        // if 9 then game ends
        if(signal.charAt(0) == '9'){
            //System.out.println(signal.charAt(1));
            String score = signal.substring(2);
            int winner = Character.getNumericValue(signal.charAt(1));

            System.out.printf("Your Score: %s\n", score);
            if(winner == id){
                System.out.println("You won!");
            }else if(winner == 0){
                System.out.println("Draw");
            }else{
                System.out.println("You lose");
            }
            // Terminate The Program
            System.exit(0);
        }


    }

    public void chooseDirection(int[] direction){

        StringBuffer msg = new StringBuffer("Direction(s): ");
        for(int i = 0; i < direction.length; i++){
            if(direction[i] == 1){
                switch(i){
                    case 0:
                        msg.append("NORTH(w) "); break;
                    case 1:
                        msg.append("WEST(a) "); break;
                    case 2:
                        msg.append("EAST(d) "); break;
                    case 3:
                        msg.append("SOUTH(s)"); break;
                }
            }
        }
        System.out.println(msg.toString());

        boolean validate = false;
        String choice = "";
        while(!validate){
            System.out.print("Choice>> ");
            choice = scan.next();
            validate = validateInput(choice, direction);
        }

        int dir = 0;
        switch(choice.toUpperCase()){
            case "W":
                dir = 1; break;
            case "A":
                dir = 2; break;
            case "D":
                dir = 3; break;
            case "S":
                dir = 4; break;
        }

        // SEND
        String buffer = String.valueOf(dir);
        try{
            Send.write(Arrays.copyOf(buffer.getBytes(), buffer.length() + 1));
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    // Validate whether the given input is true
    public boolean validateInput(String input, int[] direction){

        switch(input.toUpperCase()){
            // NORTH
            case "W":
                if(direction[0] == 1){
                    return true;
                }
                break;
            case "A":
                if(direction[1] == 1){
                    return true;
                }
                break;
            case "D":
                if(direction[2] == 1){
                    return true;
                }
                break;
            case "S":
                if(direction[3] == 1){
                    return true;
                }
                break;
            // for some unknown input
            default:
                return false;
        }
        return false;
    }

    public static void main(String[] args){

        if(args.length < 1){
            System.out.println("Missing Hostname");
            System.exit(1);
        }

        GridGameClient client = new GridGameClient(args[0]);

        client.connect();

        client.doGame();

    }
}
