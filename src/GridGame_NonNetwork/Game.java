package GridGame_NonNetwork;

import java.util.Random;

/**
 * Created by spaism on 2/19/15.
 */

/*
    TODO: Change Sending Command of 6 when Player is placed to a coordinate. This will not solve the problem directly, but add new method in NetworkPlayer for PlayerConnected() will check whether the user was told to start the game or not. if this is false send, if true then never call the method any more.
 */

public class Game {

    // Class Field Variables

    // -1 as obstacle, 0 as nothing, 1 as user1, 2 as user2, 5 as treasure
    protected int[][] grid = {
            {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},// 0
            {-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1},// 1
            {-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1},// 2
            {-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1},// 3
            {-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1},// 4
            {-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1},// 5
            {-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1},// 6
            {-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1},// 7
            {-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1},// 8
            {-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1},// 9
            {-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1},// 10
            {-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1},// 11
            {-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1},// 12
            {-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1},// 13
            {-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1},// 14
            {-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1},// 15
            {-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1},// 16
            {-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1},// 17
            {-1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -1},// 18
            {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1}// 19

    };

    protected int treasureAmount = 1;
    private Player player1, player2;
    protected int Turn = 1;

    private long RandomSeed;

    // Constructor
    public Game(){
        // Generate Seed for Random from currentTimeMills
        RandomSeed = System.currentTimeMillis();

        // Place Obstacles
        randomizeObstacle();

        // Place Treasures
        randomizeTreasure();
    }

    public Game(Player p1, Player p2){

        // Generate Seed for Random from currentTimeMills
        RandomSeed = System.currentTimeMillis();

        // Place Obstacles
        randomizeObstacle();

        // Place Treasures
        randomizeTreasure();

        // variable to determine whether the player was placed where 0 was originally located.
        boolean locationSecure = false;
        // x and y coordinate of player
        int xCoord = 1, yCoord = 1;
        // Generate Random Number for player position
        while(!locationSecure) {
            xCoord = generateRandomNum();
            yCoord = generateRandomNum();
            // if grid of allocated x and y is secure, then change locationSecure to true.
            locationSecure = secureLocation(xCoord, yCoord);
        }

        // Set x and y coordinate of the player
        p1.setCoordinate(xCoord, yCoord);
        // Add Player1's position
        grid[yCoord][xCoord] = 1;
        System.out.println("Player1 Position(" + xCoord + ", " + yCoord + ")");

        player1 = p1;
        locationSecure = false;

        while(!locationSecure) {
            xCoord = generateRandomNum();
            yCoord = generateRandomNum();
            // if grid of allocated x and y is secure, then change locationSecure to true.
            locationSecure = secureLocation(xCoord, yCoord);
        }

        // Set x and y coordinate of the player
        p2.setCoordinate(xCoord, yCoord);
        // Add Player1's position
        grid[yCoord][xCoord] = 2;
        System.out.println("Player2 Position(" + xCoord + ", " + yCoord + ")");

        player2 = p2;

    }

    // returns boolean of whether the given grid coordinate is secure or not.
    protected boolean secureLocation(int x, int y){

        if(grid[y][x] == 0){
            return true;
        }else{
            return false;
        }

    }

    // Generate Random Number for Grid Positioning. Range: 1~18(0 and 19 are excluded)
    protected int generateRandomNum(){

        // Generate Seed
        long Seed = System.currentTimeMillis();
        // Compare Seed with RandomSeed, if same then loop until it gets different
        while(Seed == RandomSeed){
            Seed = System.currentTimeMillis();
        }
        // Reflect the New Seed to RandomSeed for Next use
        RandomSeed = Seed;

        // Maybe better to give some seed
        Random rnd = new Random(RandomSeed);
        int rndNum;

        // Generate Integer between 0 to 17(inclusive) then add 1
        // 1 through 18 is safe from obstacle
        rndNum = rnd.nextInt(18) + 1;

        return rndNum;
    }

    // Arrange Obstacles randomly
    protected void randomizeObstacle(){

        int amountObstacle = 20;
        int xCoord, yCoord;
        while(amountObstacle > 0){
            xCoord = generateRandomNum();
            yCoord = generateRandomNum();

            if(secureLocation(xCoord, yCoord) && eightWaySecure(xCoord, yCoord)){
                addObject(xCoord, yCoord, -1);
                amountObstacle--;
            }else{
                // Repeat until the amountObstacle reaches zero.
            }
        }
    }

    // Check 8 ways of direction by the given coordinate
    protected boolean eightWaySecure(int x, int y){
        // Variable: security is defined to check whether the 8 different direction is safe or not. False means not safe.
        boolean security = true;

        // NORTH WEST
        if(grid[y-1][x-1] != 0){
            security = false;
        }

        // NORTH
        if(grid[y-1][x] != 0){
            security = false;
        }

        // NORTH EAST
        if(grid[y+1][x+1] != 0){
            security = false;
        }

        // WEST
        if(grid[y][x-1] != 0){
            security = false;
        }

        // EAST
        if(grid[y][x+1] != 0){
            security = false;
        }

        // SOUTH WEST
        if(grid[y+1][x-1] != 0){
            security = false;
        }

        // SOUTH
        if(grid[y+1][x] != 0){
            security = false;
        }

        // SOUTH EAST
        if(grid[y+1][x+1] != 0){
            security = false;
        }

        // if all of the 8 direction are safe then it returns true
        return security;
    }

    // Arrange Treasures randomly
    protected void randomizeTreasure(){

        int amountTreasure = this.treasureAmount;
        int xCoord, yCoord;
        while(amountTreasure > 0){
            xCoord = generateRandomNum();
            yCoord = generateRandomNum();

            if(secureLocation(xCoord, yCoord)){
                // Generate treasure value using generateRadnomNum() Method,
                // the treasure value is kept above 5.
                // adding 4 for bias.
                // Range of treasure value (5 ~ 24 points)
                int treasureValue = generateRandomNum() + 4;
                addObject(xCoord, yCoord, treasureValue);
                amountTreasure--;
            }else{
                // Repeat util the amount of treasure reaches zero.
            }
        }
    }

    // Add Object to the given arguments coordinate with given item(integer)
    protected void addObject(int x, int y, int item){

        grid[y][x] = item;

    }

    // Start Game from here
    public void startGame(){

        // Game continues until the treasureAmount reach 0
        while(treasureAmount > 0){
            int[] availableDirection_Player1 = checkPath(player1.getCoordinate());
            String direction = player1.askDirection(availableDirection_Player1);

            // Move the player number on the grid.
            this.move(direction);
            System.out.println("Player Now at (" + player1.getCoord_X() + ", " + player1.getCoord_Y() + ")");

            // Change Turn(1 to 2)
            Turn++;

            int[] availableDirection_Player2 = checkPath(player2.getCoordinate());
            direction = player2.askDirection(availableDirection_Player2);

            this.move(direction);
            System.out.println("Player Now at (" + player2.getCoord_X() + ", " + player2.getCoord_Y() + ")");

            // Change Turn(2 to 1)
            Turn--;
        }

    }

    // Calculate the possible direction from given coordinate
    protected int[] checkPath(int[] coordinate){

        // path ordered in [NORTH, WEST, EAST, SOUTH];
        // 0 for no direction, 1 for yes direction
        int[] path = {0, 0, 0, 0};
        int x = coordinate[0];
        int y = coordinate[1];

        //System.out.printf("Checking Coordinate(%d, %d)\n", x, y);
        //lookupArray(1);

        // Check North
        try{
            if(grid[y - 1][x] != -1){
                path[0] = 1;
            }else{
                path[0] = 0;
            }
        }catch(ArrayIndexOutOfBoundsException e){
            System.out.println("ArrayIndexOutOfBounds");
            path[0] = 0;
        }

        // Check West
        try{
            if(grid[y][x - 1] != -1){
                path[1] = 1;
            }else{
                path[1] = 0;
            }
        }catch(ArrayIndexOutOfBoundsException e){
            System.out.println("ArrayIndexOutOfBounds");
            path[1] = 0;
        }

        // Check East
        try{
            if(grid[y][x + 1] != -1){
                path[2] = 1;
            }else{
                path[2] = 0;
            }
        }catch(ArrayIndexOutOfBoundsException e){
            System.out.println("ArrayIndexOutOfBounds");
            path[2] = 0;
        }

        // Check South
        try{
            if(grid[y + 1][x] != -1){
                path[3] = 1;
            }else{
                path[3] = 0;
            }
        }catch(ArrayIndexOutOfBoundsException e){
            System.out.println("ArrayIndexOutOfBounds");
            path[3] = 0;
        }

        return path;
    }

    // Move the player with given direction
    protected void move(String direction){

        int curX, curY;

        if(Turn == 1){
            curX = player1.getCoord_X();
            curY = player1.getCoord_Y();
        }else{
            curX = player2.getCoord_X();
            curY = player2.getCoord_Y();
        }

        switch(direction){
            case "NORTH":
                swap(curX, curY, curX, curY - 1);
                break;
            case "WEST":
                swap(curX, curY, curX - 1, curY);
                break;
            case "EAST":
                swap(curX, curY, curX + 1, curY);
                break;
            case "SOUTH":
                swap(curX, curY, curX, curY + 1);
                break;
        }

        // Move the player instance to specified direction
        if(Turn == 1){
            player1.move(direction);
        }else{
            player2.move(direction);
        }
    }

    // Swap elements of the Array
    protected void swap(int x1, int y1, int x2, int y2){

        System.out.printf("grid[x1][y1]=%d, grid[x2][y2]=%d\n", grid[x1][y1], grid[x2][y2]);

        // variable temp will house the destination grid value.
        int temp = grid[y2][x2];

        // if user move to a grid where Treasure is located.
        if(temp > 5){
            System.out.println("User stepped on the treasure!");
            if(Turn == 1){
                player1.addScore(temp);
                System.out.printf("Player 1's Score:%d\n", player1.getScore());
            }else{
                player2.addScore(temp);
                System.out.printf("Player 2's Score:%d\n", player2.getScore());
            }

            temp = 0;
            treasureAmount--;
        }

        // if user overlap with other player
        if(temp == 1 || temp == 2){
            // Mini-Game happens
            // if miniGame() returns true, then do the swap, else end the method here, no swap occurs.
            if(!miniGame()){
                return;
            }
        }

        grid[y2][x2] = grid[y1][x1];
        grid[y1][x1] = temp;

    }

    // Event when overlap occurs
    protected boolean miniGame(){

        boolean doSwap = false;

        int num_player1, num_player2;
        // Player1 overlapped with Player2
        if(Turn == 1){
            // Ask Number to Player2 the Number
            num_player2 = player2.overlapEventInteraction(0);
            // Ask Number to Player1 to guess Player2's choice
            num_player1 = player1.overlapEventInteraction(1);

            if(num_player1 == num_player2){
                System.out.println("Player1 guessed correctly!!");
                player1.addScore(999);
                placeRandom(2);
                doSwap = true;
            }else{
                System.out.println("Player1 couldn't guess...");
                placeRandom(1);
            }
        }
        // Player2 overlapped with Player1
        else{
            // Ask Number to Player1 the Number
            num_player1 = player1.overlapEventInteraction(0);
            // Ask Number to Player2 to guess Player1's choice
            num_player2 = player2.overlapEventInteraction(1);

            if(num_player1 == num_player2){
                System.out.println("Player2 guessed correctly!!");
                player2.addScore(999);
                placeRandom(1);
                doSwap = true;
            }else{
                System.out.println("Player2 couldn't guess...");
                placeRandom(2);
            }
        }

        return doSwap;
    }

    protected void placeRandom(int player){

        if(player == 1){
            // variable to determine whether the player was placed where 0 was originally located.
            boolean locationSecure = false;
            // x and y coordinate of player
            int xCoord = 1, yCoord = 1;
            // Generate Random Number for player position
            while(!locationSecure) {
                xCoord = generateRandomNum();
                yCoord = generateRandomNum();
                // if grid of allocated x and y is secure, then change locationSecure to true.
                locationSecure = secureLocation(xCoord, yCoord);
            }

            // Remove previous coordinate of Player1
            removeObject(player1.getCoord_X(), player1.getCoord_Y());

            // Set x and y coordinate of the player
            player1.setCoordinate(xCoord, yCoord);
            // Add Player1's position
            grid[yCoord][xCoord] = 1;
            System.out.println("Player1 Position(" + xCoord + ", " + yCoord + ")");

        }else if(player == 2){
            // variable to determine whether the player was placed where 0 was originally located.
            boolean locationSecure = false;
            // x and y coordinate of player
            int xCoord = 1, yCoord = 1;
            // Generate Random Number for player position
            while(!locationSecure) {
                xCoord = generateRandomNum();
                yCoord = generateRandomNum();
                // if grid of allocated x and y is secure, then change locationSecure to true.
                locationSecure = secureLocation(xCoord, yCoord);
            }

            // Remove previous coordinate of Player1
            removeObject(player2.getCoord_X(), player2.getCoord_Y());

            // Set x and y coordinate of the player
            player2.setCoordinate(xCoord, yCoord);
            // Add Player1's position
            grid[yCoord][xCoord] = 1;
            System.out.println("Player2 Position(" + xCoord + ", " + yCoord + ")");
        }
    }

    // Remove item from grid
    protected void removeObject(int x, int y){
        if(grid[y][x] != 0){
            grid[y][x] = 0;
        }
    }

}
