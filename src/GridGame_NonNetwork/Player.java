package GridGame_NonNetwork;

import java.util.Scanner;

/**
 * Created by spaism on 2/19/15.
 */
public class Player {
    protected int score;
    protected int id;
    protected int[] coordinate;
    private Scanner scan;

    // Constructor
    public Player(int id){
        this.id = id;
        this.coordinate = new int[2];
        scan = new Scanner(System.in);
    }

    // Add points scored by the player
    public void addScore(int points){
        score += points;
    }

    // Return the score of player
    public int getScore(){
        return score;
    }

    // Set players coordinate
    // This method is called only when the player is instanced from the Game class
    // movement of the player is controlled by the method, move()
    public void setCoordinate(int x, int y){
        this.coordinate[0] = x;
        this.coordinate[1] = y;
    }

    // Return the player's coordinate
    public int[] getCoordinate(){
        return coordinate;
    }

    // Return the player's x coordinate
    public int getCoord_X(){
        return this.coordinate[0];
    }

    // Return the player's y coordinate
    public int getCoord_Y(){
        return this.coordinate[1];
    }

    // Move the player coordinate by given direction
    public void move(String direction){
        switch(direction){
            case "NORTH":
                this.coordinate[1] -= 1;
                break;
            case "WEST":
                this.coordinate[0] -= 1;
                break;
            case "EAST":
                this.coordinate[0] += 1;
                break;
            case "SOUTH":
                this.coordinate[1] += 1;
                break;
            default:
                System.out.println("Undefined Direction");
                break;
        }
    }

    // User interaction: Ask user for a direction. Interrogate with given moveChoice[]
    public String askDirection(int[] moveChoice){
        // moveChoice[NORTH, WEST, EAST, SOUTH]

        StringBuffer msg = new StringBuffer("You can move ");

        for(int i = 0; i < 4; i++){
            if(moveChoice[i] == 1){
                switch(i){
                    case 0:
                        msg.append("NORTH "); break;
                    case 1:
                        msg.append("WEST "); break;
                    case 2:
                        msg.append("EAST "); break;
                    case 3:
                        msg.append("SOUTH"); break;
                }
            }
        }

        System.out.println(msg.toString());

        String input = "";
        while(input.equals("")){
            System.out.printf("Player%d Direction> ", id);
            input = scan.next();

            // Check if the input was correct
            if((input = checkInput(input, moveChoice)) != null){
                // If correct, return the retured value
                break;
            }else{
                // If wrong, ask again for direction
                input = "";
            }
        }
        return input;
    }

    // Validate the user's input of direction, if mal-formatted then return null for retry.
    public String checkInput(String input, int[] moveChoice){

        input = input.toUpperCase();

        if(input.equals("NORTH")){
            if(moveChoice[0] == 1){
                return input;
            }else{
                return null;
            }
        }

        if(input.equals("WEST")){
            if(moveChoice[1] == 1){
                return input;
            }else{
                return null;
            }
        }

        if(input.equals("EAST")){
            if(moveChoice[2] == 1){
                return input;
            }else{
                return null;
            }
        }

        if(input.equals("SOUTH")){
            if(moveChoice[3] == 1){
                return input;
            }else{
                return null;
            }
        }

        if(input.equals("W")){
            if(moveChoice[0] == 1){
                return "NORTH";
            }else{
                return null;
            }
        }

        if(input.equals("A")){
            if(moveChoice[1] == 1){
                return "WEST";
            }else{
                return null;
            }
        }

        if(input.equals("D")){
            if(moveChoice[2] == 1){
                return "EAST";
            }else{
                return null;
            }
        }

        if(input.equals("S")){
            if(moveChoice[3] == 1){
                return "SOUTH";
            }else{
                return null;
            }
        }

        return null;
    }

    // When players overlapped this method is called, Game Class's miniGame() will call this.
    public int overlapEventInteraction(int overlapped){

        StringBuffer message = new StringBuffer("OVERLAP EVENT!\n");

        if(overlapped == 0){
            message.append("You were overlapped by other Player!\n");
            message.append("Choose a number from 0 to 9 and defend yourself!");
        }else{
            message.append("You overlapped with other player!\n");
            message.append("Choose a number from 0 to 9 to Earn Extra Points!");
        }

        System.out.println(message.toString());

        boolean correctInput = false;
        int input = 0;
        Scanner scan = new Scanner(System.in);

        while(!correctInput){
            System.out.print("Number>>> ");
            input = scan.nextInt();

            if(input >= 0 && input <= 9){
                correctInput = true;
            }
        }

        return input;
    }
}
