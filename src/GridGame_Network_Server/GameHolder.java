package GridGame_Network_Server;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

/**
 * Created by spaism on 2/21/15.
 */
public class GameHolder extends Thread{

    InputStream iStream;
    OutputStream oStream;
    StreamHolder holder_alice, holder_bob;
    Socket alice, bob;

    public GameHolder(Socket client_Alice, Socket client_Bob){

        try{
            bob = client_Bob;
            iStream = client_Alice.getInputStream();
            oStream = client_Alice.getOutputStream();
            holder_alice = new StreamHolder(iStream,oStream);

            alice = client_Alice;
            iStream = client_Bob.getInputStream();
            oStream = client_Bob.getOutputStream();
            holder_bob = new StreamHolder(iStream,oStream);

        }catch(Exception e){
            System.out.println("ERROR<GameHolder>: Constructor encountered an error");
        }
    }

    public void run(){

        try{
            NetworkPlayer np_alice = new NetworkPlayer(1, holder_alice.getInStream(), holder_alice.getOutStream());
            NetworkPlayer np_bob = new NetworkPlayer(2, holder_bob.getInStream(), holder_bob.getOutStream());

            // Send Signal for Starting Game
            np_alice.sendStartSignal();
            np_bob.sendStartSignal();

            GameCore game = new GameCore(np_alice, np_bob);

            game.startGame();

        }catch(Exception e){

        }
    }
}

class StreamHolder{

    private InputStream iStream;
    private OutputStream oStream;

    public StreamHolder(InputStream input, OutputStream output){
        iStream = input;
        oStream = output;
    }

    public InputStream getInStream(){
        return iStream;
    }

    public OutputStream getOutStream(){
        return oStream;
    }

}