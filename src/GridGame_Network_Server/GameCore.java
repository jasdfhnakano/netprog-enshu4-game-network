package GridGame_Network_Server;

import GridGame_NonNetwork.Game;

import java.util.Arrays;

/**
 * Created by spaism on 2/21/15.
 */
public class GameCore extends Game {

    NetworkPlayer np_1, np_2;

    public GameCore(NetworkPlayer p1, NetworkPlayer p2){
        // Call no argument Constructor of Super Class
        super();

        boolean locationSecure = false;

        int xCoord = 1, yCoord = 1;

        while(!locationSecure){
            xCoord = generateRandomNum();
            yCoord = generateRandomNum();

            locationSecure = secureLocation(xCoord, yCoord);
        }

        p1.setCoordinate(xCoord, yCoord);

        grid[yCoord][xCoord] = 1;
        System.out.println("Player1 Position(" + xCoord + ", " + yCoord + ")");

        np_1 = p1;

        locationSecure = false;

        while(!locationSecure){
            xCoord = generateRandomNum();
            yCoord = generateRandomNum();

            locationSecure = secureLocation(xCoord, yCoord);
        }

        p2.setCoordinate(xCoord, yCoord);

        grid[yCoord][xCoord] = 2;
        System.out.println("Player2 Position(" + xCoord + ", " + yCoord + ")");

        np_2 = p2;

    }

    @Override
    public void startGame(){

        while(treasureAmount > 0){
            int[] availableDirection_Player1 = checkPath(np_1.getCoordinate());
            String direction = np_1.askDirection(availableDirection_Player1);

            // Move the player number on the grid.
            this.move(direction);
            System.out.println("Player Now at (" + np_1.getCoord_X() + ", " + np_1.getCoord_Y() + ")");

            // Change Turn(1 to 2)
            Turn++;
            showGrid();

            int[] availableDirection_Player2 = checkPath(np_2.getCoordinate());
            direction = np_2.askDirection(availableDirection_Player2);

            this.move(direction);
            System.out.println("Player Now at (" + np_2.getCoord_X() + ", " + np_2.getCoord_Y() + ")");

            // Change Turn(2 to 1)
            Turn--;
            showGrid();
        }

        // Compare Score
        int player1 = np_1.getScore();
        int player2 = np_2.getScore();
        int winner;
        if(player1 < player2){
            winner = 2;
        }else if(player1 > player2){
            winner = 1;
        }else{
            winner = 0;
        }

        String buffer_1 = "9";
        String buffer_2 = "9";
        buffer_1 += String.valueOf(winner);
        buffer_2 += String.valueOf(winner);
        buffer_1 += String.valueOf(np_1.getScore());
        buffer_2 += String.valueOf(np_2.getScore());
        try{
            np_1.Send.write(Arrays.copyOf(buffer_1.getBytes(), buffer_1.length()));
            np_2.Send.write(Arrays.copyOf(buffer_2.getBytes(), buffer_2.length()));
        }catch(Exception e){

        }

    }

    @Override
    protected  void move(String direction){

        int curX, curY;

        if(Turn == 1){
            curX = np_1.getCoord_X();
            curY = np_1.getCoord_Y();
        }else{
            curX = np_2.getCoord_X();
            curY = np_2.getCoord_Y();
        }

        switch(direction){
            case "NORTH":
                swap(curX, curY, curX, curY - 1);
                break;
            case "WEST":
                swap(curX, curY, curX - 1, curY);
                break;
            case "EAST":
                swap(curX, curY, curX + 1, curY);
                break;
            case "SOUTH":
                swap(curX, curY, curX, curY + 1);
                break;
        }

        // Move the player instance to specified direction
        if(Turn == 1){
            np_1.move(direction);
        }else{
            np_2.move(direction);
        }

    }

    @Override
    protected void swap(int x1, int y1, int x2, int y2){

        // variable temp will house the destination grid value.
        int temp = grid[y2][x2];

        // if user move to a grid where Treasure is located.
        if(temp > 5){
            System.out.println("User stepped on the treasure!");
            if(Turn == 1){
                np_1.addScore(temp);
                System.out.printf("Player 1's Score:%d\n", np_1.getScore());
            }else{
                np_2.addScore(temp);
                System.out.printf("Player 2's Score:%d\n", np_2.getScore());
            }

            temp = 0;
            treasureAmount--;
        }

        // if user overlap with other player
        if(temp == 1 || temp == 2){
            // if miniGame() returns true, then do the swap, else end the method here, no swap occurs.
            if(!miniGame()){
                return;
            }
            temp = grid[y2][x2];
        }

        // swap process
        grid[y2][x2] = grid[y1][x1];
        grid[y1][x1] = temp;
    }


    protected boolean miniGame(){

        boolean doSwap = false;

        int num_player1, num_player2;
        // Player1 overlapped with Player2
        if(Turn == 1){
            // Ask Number to Player2 the Number
            num_player2 = np_2.overlapEventInteraction(0);
            // Ask Number to Player1 to guess Player2's choice
            num_player1 = np_1.overlapEventInteraction(1);

            if(num_player1 == num_player2){
                System.out.println("Player1 guessed correctly!!");
                np_1.addScore(999);
                placeRandom(2);
                doSwap = true;
            }else{
                System.out.println("Player1 couldn't guess...");
                placeRandom(1);
            }
        }
        // Player2 overlapped with Player1
        else{
            // Ask Number to Player1 the Number
            num_player1 = np_1.overlapEventInteraction(0);
            // Ask Number to Player2 to guess Player1's choice
            num_player2 = np_2.overlapEventInteraction(1);

            if(num_player2 == num_player1){
                System.out.println("Player2 guessed correctly!!");
                np_2.addScore(999);
                placeRandom(1);
                doSwap = true;
            }else{
                System.out.println("Player2 couldn't guess...");
                placeRandom(2);
            }
        }

        return doSwap;
    }

    @Override
    // Place the given player to random place
    protected void placeRandom(int player){

        if(player == 1){
            // variable to determine whether the player was placed where 0 was originally located.
            boolean locationSecure = false;
            // x and y coordinate of player
            int xCoord = 1, yCoord = 1;
            // Generate Random Number for player position
            while(!locationSecure) {
                xCoord = generateRandomNum();
                yCoord = generateRandomNum();
                // if grid of allocated x and y is secure, then change locationSecure to true.
                locationSecure = secureLocation(xCoord, yCoord);
            }

            // Remove previous coordinate of Player1
            removeObject(np_1.getCoord_X(), np_1.getCoord_Y());

            // Set x and y coordinate of the player
            np_1.setCoordinate(xCoord, yCoord);
            // Add Player1's position
            grid[yCoord][xCoord] = 1;
            System.out.println("Player1 Position(" + xCoord + ", " + yCoord + ")");

        }else if(player == 2){
            // variable to determine whether the player was placed where 0 was originally located.
            boolean locationSecure = false;
            // x and y coordinate of player
            int xCoord = 1, yCoord = 1;
            // Generate Random Number for player position
            while(!locationSecure) {
                xCoord = generateRandomNum();
                yCoord = generateRandomNum();
                // if grid of allocated x and y is secure, then change locationSecure to true.
                locationSecure = secureLocation(xCoord, yCoord);
            }

            // Remove previous coordinate of Player1
            removeObject(np_2.getCoord_X(), np_2.getCoord_Y());

            // Set x and y coordinate of the player
            np_2.setCoordinate(xCoord, yCoord);
            // Add Player1's position
            grid[yCoord][xCoord] = 2;
            System.out.println("Player2 Position(" + xCoord + ", " + yCoord + ")");
        }
    }

    // Show Entire Grid Status
    protected void showGrid(){
        for(int y = 0; y < 20; y++){
            for(int x = 0; x < 20; x++){
                System.out.printf("%2d ", grid[y][x]);
            }
            System.out.println();
        }
    }
}
