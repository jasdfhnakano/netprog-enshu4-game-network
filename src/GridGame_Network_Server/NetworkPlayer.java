package GridGame_Network_Server;

import GridGame_NonNetwork.Player;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;

/**
 * Created by spaism on 2/20/15.
 */
public class NetworkPlayer extends Player {

    protected InputStream Receive;
    protected OutputStream Send;

    public NetworkPlayer(int id, InputStream input, OutputStream output){
        // call Super Class Constructor. Scanner not necessary... Excess use of memory may occur.
        super(id);
        Receive = input;
        Send = output;
    }

    @Override
    public String askDirection(int[] moveChoice){

        StringBuffer choice_sb = new StringBuffer("3");

        for(int i = 0; i < 4; i++){
            choice_sb.append(String.valueOf(moveChoice[i]));
        }

        choice_sb.append(String.valueOf(super.getScore()));

        String choice = choice_sb.toString();
        try {
            Send.write(Arrays.copyOf(choice.getBytes("UTF-8"), choice.length()));

            int n;
            byte[] buffer = new byte[128];
            n = Receive.read(buffer);
            choice = new String(buffer, 0, n, "UTF-8");
            System.out.printf("choice %s\n", choice);
            switch(choice.charAt(0)){
                case '1':
                    choice = "NORTH";
                    break;
                case '2':
                    choice = "WEST";
                    break;
                case '3':
                    choice = "EAST";
                    break;
                case '4':
                    choice = "SOUTH";
                    break;
                }

        }catch(Exception e){
            e.printStackTrace();
            System.out.println("ERROR<NetworkPlayer>: Encountered an Error");
        }

        // CheckInput is not called, since this kind of process is already done in Client

        return choice;
    }

    @Override
    public int overlapEventInteraction(int overlapped){

        // Variable number must be initialized to return, Default as 0
        int number = 0;

        StringBuffer overlap_sb = new StringBuffer("4");

        try{
            Send.write(Arrays.copyOf((overlap_sb.toString()).getBytes("UTF-8"), (overlap_sb.length()) + 1));

            byte[] buffer = new byte[128];
            int n = Receive.read(buffer);

            String choice = new String(buffer, 0, n, "UTF-8");
            if(choice.charAt(0) == '4'){
                number = Character.getNumericValue(choice.charAt(1));
            }
        }catch(Exception e){
            System.out.println();
        }

        return number;
    }

    public void sendStartSignal(){
        String signal = "6";
        try{
            Send.write(Arrays.copyOf(signal.getBytes(), signal.length()));
        }catch(Exception e){
            System.out.println("ERROR: Sending Signal Failed");
        }
    }
}
