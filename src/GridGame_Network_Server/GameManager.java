package GridGame_Network_Server;

import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Arrays;

/**
 * Created by spaism on 2/20/15.
 */
public class GameManager {

    // Game transaction through PORT 60033
    static final int PORT = 60033;
    // Amount of Buffer size, minimum is best
    static final int BUFSIZ = 128;

    public static void main(String[] args){

        Socket clientSocket_A, clientSocket_B;
        OutputStream outStream_a, outStream_b;
        byte[] buffer = new byte[BUFSIZ];

        try{
            ServerSocket serverSocket = new ServerSocket(PORT);

            while(true){
                clientSocket_A = null;
                clientSocket_B = null;
                System.out.println("Server Initialized");
                String message = "";

                clientSocket_A = serverSocket.accept();
                System.out.println("Client 1 hooked up");
                message = "1";
                outStream_a = clientSocket_A.getOutputStream();
                outStream_a.write(Arrays.copyOf(message.getBytes("UTF-8"), 1));

                clientSocket_B = serverSocket.accept();
                System.out.println("Client 2 hooked up");
                message = "2";
                outStream_b = clientSocket_B.getOutputStream();
                outStream_b.write(Arrays.copyOf(message.getBytes("UTF-8"), 1));

                GameHolder gh = new GameHolder(clientSocket_A, clientSocket_B);
                gh.start();
                System.out.println("Game Opened");
            }
        }catch(Exception e){
            System.out.println("Server Encountered an Error");
        }
    }
}
